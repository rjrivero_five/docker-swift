FROM phusion/baseimage:latest

MAINTAINER rjrivero

# Let the container know that there is no TTY
ENV DEBIAN_FRONTEND noninteractive

# Install dependencies.
RUN apt-get -y update && \
    apt-get install -y ubuntu-cloud-keyring && \
    echo "deb http://ubuntu-cloud.archive.canonical.com/ubuntu" "trusty-updates/liberty main" > /etc/apt/sources.list.d/cloudarchive-liberty.list && \
    apt-get -y update && \
    apt-get -y install mysql-server mysql-client python-mysqldb \
                       openssl keystone python-openstackclient  \
                       swift swift-proxy python-swiftclient \
                       python-keystoneclient python-keystonemiddleware \
                       swift-account swift-container swift-object \
                       apache2 libapache2-mod-wsgi  \ 
                       memcached python-memcache && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# -------------------
# Setting up Keystone
# -------------------

# Add Keystone config
ADD files/keystone/keystone.conf /etc/keystone/keystone.conf

# Remove default listen ports, only 5000 and 35357 are required
ADD files/apache2/wsgi-keystone.conf \
    /etc/apache2/sites-enabled/wsgi-keystone.conf
RUN rm -f /etc/apache2/ports.conf && \
    rm -f /etc/apache2/sites-enabled/000-default.conf && \
    touch /etc/apache2/ports.conf && \
    mkdir /etc/default/swift.d

# ----------------
# Setting up Swift
# ----------------

# Add Swift configs
ADD files/swift/ /etc/swift
ADD files/admin-openrc.sh /root/admin-openrc.sh
ADD files/demo-openrc.sh  /root/demo-openrc.sh
RUN mkdir -p /srv/node && \
    mkdir -p /var/cache/swift && \
    chown -R swift:swift /srv/node && \
    chown -R root:swift /var/cache/swift && \
    chown -R root:swift /etc/swift

# ------------------
# Bootstrapping both
# ------------------

# Bootstrap database
ADD files/bootstrap.sh /bootstrap.sh
RUN /bootstrap.sh && rm -f /bootstrap.sh

# Add services
ADD files/service/ /etc/service/

# Expose Swift port
EXPOSE 5000 8080 35357
